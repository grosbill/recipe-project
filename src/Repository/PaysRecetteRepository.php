<?php

namespace App\Repository;

use App\Entity\PaysRecette;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PaysRecette|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaysRecette|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaysRecette[]    findAll()
 * @method PaysRecette[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaysRecetteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaysRecette::class);
    }

    // /**
    //  * @return PaysRecette[] Returns an array of PaysRecette objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaysRecette
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
