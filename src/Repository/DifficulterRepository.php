<?php

namespace App\Repository;

use App\Entity\Difficulter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Difficulter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Difficulter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Difficulter[]    findAll()
 * @method Difficulter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DifficulterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Difficulter::class);
    }

    // /**
    //  * @return Difficulter[] Returns an array of Difficulter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Difficulter
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
