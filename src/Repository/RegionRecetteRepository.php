<?php

namespace App\Repository;

use App\Entity\RegionRecette;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RegionRecette|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegionRecette|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegionRecette[]    findAll()
 * @method RegionRecette[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegionRecetteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegionRecette::class);
    }

    // /**
    //  * @return RegionRecette[] Returns an array of RegionRecette objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegionRecette
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
