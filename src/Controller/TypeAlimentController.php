<?php

namespace App\Controller;

use App\Entity\TypeAliment;
use App\Form\TypeAlimentType;
use App\Repository\TypeAlimentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type/aliment")
 */
class TypeAlimentController extends AbstractController
{
    /**
     * @Route("/", name="type_aliment_index", methods={"GET"})
     */
    public function index(TypeAlimentRepository $typeAlimentRepository): Response
    {
        return $this->render('type_aliment/index.html.twig', [
            'type_aliments' => $typeAlimentRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="type_aliment_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typeAliment = new TypeAliment();
        $form = $this->createForm(TypeAlimentType::class, $typeAliment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeAliment);
            $entityManager->flush();

            return $this->redirectToRoute('type_aliment_index');
        }

        return $this->render('type_aliment/new.html.twig', [
            'type_aliment' => $typeAliment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_aliment_show", methods={"GET"})
     */
    public function show(TypeAliment $typeAliment): Response
    {
        return $this->render('type_aliment/show.html.twig', [
            'type_aliment' => $typeAliment,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_aliment_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypeAliment $typeAliment): Response
    {
        $form = $this->createForm(TypeAlimentType::class, $typeAliment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_aliment_index');
        }

        return $this->render('type_aliment/edit.html.twig', [
            'type_aliment' => $typeAliment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_aliment_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypeAliment $typeAliment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeAliment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeAliment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('type_aliment_index');
    }
}
