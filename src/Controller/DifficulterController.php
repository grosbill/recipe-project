<?php

namespace App\Controller;

use App\Entity\Difficulter;
use App\Form\DifficulterType;
use App\Repository\DifficulterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/difficulter")
 */
class DifficulterController extends AbstractController
{
    /**
     * @Route("/", name="difficulter_index", methods={"GET"})
     */
    public function index(DifficulterRepository $difficulterRepository): Response
    {
        return $this->render('difficulter/index.html.twig', [
            'difficulters' => $difficulterRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="difficulter_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $difficulter = new Difficulter();
        $form = $this->createForm(DifficulterType::class, $difficulter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($difficulter);
            $entityManager->flush();

            return $this->redirectToRoute('difficulter_index');
        }

        return $this->render('difficulter/new.html.twig', [
            'difficulter' => $difficulter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="difficulter_show", methods={"GET"})
     */
    public function show(Difficulter $difficulter): Response
    {
        return $this->render('difficulter/show.html.twig', [
            'difficulter' => $difficulter,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="difficulter_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Difficulter $difficulter): Response
    {
        $form = $this->createForm(DifficulterType::class, $difficulter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('difficulter_index');
        }

        return $this->render('difficulter/edit.html.twig', [
            'difficulter' => $difficulter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="difficulter_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Difficulter $difficulter): Response
    {
        if ($this->isCsrfTokenValid('delete'.$difficulter->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($difficulter);
            $entityManager->flush();
        }

        return $this->redirectToRoute('difficulter_index');
    }
}
