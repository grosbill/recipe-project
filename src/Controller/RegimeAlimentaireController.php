<?php

namespace App\Controller;

use App\Entity\RegimeAlimentaire;
use App\Form\RegimeAlimentaireType;
use App\Repository\RegimeAlimentaireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/regime/alimentaire")
 */
class RegimeAlimentaireController extends AbstractController
{
    /**
     * @Route("/", name="regime_alimentaire_index", methods={"GET"})
     */
    public function index(RegimeAlimentaireRepository $regimeAlimentaireRepository): Response
    {
        return $this->render('regime_alimentaire/index.html.twig', [
            'regime_alimentaires' => $regimeAlimentaireRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="regime_alimentaire_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $regimeAlimentaire = new RegimeAlimentaire();
        $form = $this->createForm(RegimeAlimentaireType::class, $regimeAlimentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($regimeAlimentaire);
            $entityManager->flush();

            return $this->redirectToRoute('regime_alimentaire_index');
        }

        return $this->render('regime_alimentaire/new.html.twig', [
            'regime_alimentaire' => $regimeAlimentaire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="regime_alimentaire_show", methods={"GET"})
     */
    public function show(RegimeAlimentaire $regimeAlimentaire): Response
    {
        return $this->render('regime_alimentaire/show.html.twig', [
            'regime_alimentaire' => $regimeAlimentaire,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="regime_alimentaire_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, RegimeAlimentaire $regimeAlimentaire): Response
    {
        $form = $this->createForm(RegimeAlimentaireType::class, $regimeAlimentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('regime_alimentaire_index');
        }

        return $this->render('regime_alimentaire/edit.html.twig', [
            'regime_alimentaire' => $regimeAlimentaire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="regime_alimentaire_delete", methods={"DELETE"})
     */
    public function delete(Request $request, RegimeAlimentaire $regimeAlimentaire): Response
    {
        if ($this->isCsrfTokenValid('delete'.$regimeAlimentaire->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($regimeAlimentaire);
            $entityManager->flush();
        }

        return $this->redirectToRoute('regime_alimentaire_index');
    }
}
