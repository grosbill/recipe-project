<?php

namespace App\Controller;

use App\Entity\RegionRecette;
use App\Form\RegionRecetteType;
use App\Repository\RegionRecetteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/region/recette")
 */
class RegionRecetteController extends AbstractController
{
    /**
     * @Route("/", name="region_recette_index", methods={"GET"})
     */
    public function index(RegionRecetteRepository $regionRecetteRepository): Response
    {
        return $this->render('region_recette/index.html.twig', [
            'region_recettes' => $regionRecetteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="region_recette_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $regionRecette = new RegionRecette();
        $form = $this->createForm(RegionRecetteType::class, $regionRecette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($regionRecette);
            $entityManager->flush();

            return $this->redirectToRoute('region_recette_index');
        }

        return $this->render('region_recette/new.html.twig', [
            'region_recette' => $regionRecette,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="region_recette_show", methods={"GET"})
     */
    public function show(RegionRecette $regionRecette): Response
    {
        return $this->render('region_recette/show.html.twig', [
            'region_recette' => $regionRecette,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="region_recette_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, RegionRecette $regionRecette): Response
    {
        $form = $this->createForm(RegionRecetteType::class, $regionRecette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('region_recette_index');
        }

        return $this->render('region_recette/edit.html.twig', [
            'region_recette' => $regionRecette,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="region_recette_delete", methods={"DELETE"})
     */
    public function delete(Request $request, RegionRecette $regionRecette): Response
    {
        if ($this->isCsrfTokenValid('delete'.$regionRecette->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($regionRecette);
            $entityManager->flush();
        }

        return $this->redirectToRoute('region_recette_index');
    }
}
