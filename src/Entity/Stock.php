<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 */
class Stock
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $valeurstock;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="stock")
     */
    private $users;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValeurstock(): ?float
    {
        return $this->valeurstock;
    }

    public function setValeurstock(?float $valeurstock): self
    {
        $this->valeurstock = $valeurstock;

        return $this;
    }

    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }
}
