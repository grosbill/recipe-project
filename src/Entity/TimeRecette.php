<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TimeRecetteRepository")
 */
class TimeRecette
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="time")
     */
    private $timepreparation;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $timecuisson;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Recettes", mappedBy="timerecette")
     */
    private $recettes;

    public function __construct()
    {
        $this->recettes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimepreparation(): ?\DateTimeInterface
    {
        return $this->timepreparation;
    }

    public function setTimepreparation(\DateTimeInterface $timepreparation): self
    {
        $this->timepreparation = $timepreparation;

        return $this;
    }

    public function getTimecuisson(): ?\DateTimeInterface
    {
        return $this->timecuisson;
    }

    public function setTimecuisson(?\DateTimeInterface $timecuisson): self
    {
        $this->timecuisson = $timecuisson;

        return $this;
    }

    /**
     * @return Collection|Recettes[]
     */
    public function getRecettes(): Collection
    {
        return $this->recettes;
    }

    public function addRecette(Recettes $recette): self
    {
        if (!$this->recettes->contains($recette)) {
            $this->recettes[] = $recette;
            $recette->setTimerecette($this);
        }

        return $this;
    }

    public function removeRecette(Recettes $recette): self
    {
        if ($this->recettes->contains($recette)) {
            $this->recettes->removeElement($recette);
            // set the owning side to null (unless already changed)
            if ($recette->getTimerecette() === $this) {
                $recette->setTimerecette(null);
            }
        }

        return $this;
    }
}
