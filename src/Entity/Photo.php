<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PhotoRepository")
 */
class Photo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adressephoto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Recettes", inversedBy="photo")
     * @ORM\JoinColumn(nullable=false)
     */
    private $recettes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdressephoto(): ?string
    {
        return $this->adressephoto;
    }

    public function setAdressephoto(?string $adressephoto): self
    {
        $this->adressephoto = $adressephoto;

        return $this;
    }

    public function getRecettes(): ?Recettes
    {
        return $this->recettes;
    }

    public function setRecettes(?Recettes $recettes): self
    {
        $this->recettes = $recettes;

        return $this;
    }
}
