<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlimentsRepository")
 */
class Aliments
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nomaliment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomaliment(): ?string
    {
        return $this->nomaliment;
    }

    public function setNomaliment(string $nomaliment): self
    {
        $this->nomaliment = $nomaliment;

        return $this;
    }
}
