<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AllergenesRepository")
 */
class Allergenes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $cerales;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $crustaces;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $oeufs;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $poissons;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $arachides;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $soja;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $lait;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $fruitsacoques;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $celeri;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $moutarde;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $grainedesesame;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $anhydridesulfite;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $lupin;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mollusques;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCerales(): ?string
    {
        return $this->cerales;
    }

    public function setCerales(?string $cerales): self
    {
        $this->cerales = $cerales;

        return $this;
    }

    public function getCrustaces(): ?string
    {
        return $this->crustaces;
    }

    public function setCrustaces(?string $crustaces): self
    {
        $this->crustaces = $crustaces;

        return $this;
    }

    public function getOeufs(): ?string
    {
        return $this->oeufs;
    }

    public function setOeufs(?string $oeufs): self
    {
        $this->oeufs = $oeufs;

        return $this;
    }

    public function getPoissons(): ?string
    {
        return $this->poissons;
    }

    public function setPoissons(?string $poissons): self
    {
        $this->poissons = $poissons;

        return $this;
    }

    public function getArachides(): ?string
    {
        return $this->arachides;
    }

    public function setArachides(?string $arachides): self
    {
        $this->arachides = $arachides;

        return $this;
    }

    public function getSoja(): ?string
    {
        return $this->soja;
    }

    public function setSoja(?string $soja): self
    {
        $this->soja = $soja;

        return $this;
    }

    public function getLait(): ?string
    {
        return $this->lait;
    }

    public function setLait(?string $lait): self
    {
        $this->lait = $lait;

        return $this;
    }

    public function getFruitsacoques(): ?string
    {
        return $this->fruitsacoques;
    }

    public function setFruitsacoques(?string $fruitsacoques): self
    {
        $this->fruitsacoques = $fruitsacoques;

        return $this;
    }

    public function getCeleri(): ?string
    {
        return $this->celeri;
    }

    public function setCeleri(?string $celeri): self
    {
        $this->celeri = $celeri;

        return $this;
    }

    public function getMoutarde(): ?string
    {
        return $this->moutarde;
    }

    public function setMoutarde(?string $moutarde): self
    {
        $this->moutarde = $moutarde;

        return $this;
    }

    public function getGrainedesesame(): ?string
    {
        return $this->grainedesesame;
    }

    public function setGrainedesesame(?string $grainedesesame): self
    {
        $this->grainedesesame = $grainedesesame;

        return $this;
    }

    public function getAnhydridesulfite(): ?string
    {
        return $this->anhydridesulfite;
    }

    public function setAnhydridesulfite(?string $anhydridesulfite): self
    {
        $this->anhydridesulfite = $anhydridesulfite;

        return $this;
    }

    public function getLupin(): ?string
    {
        return $this->lupin;
    }

    public function setLupin(?string $lupin): self
    {
        $this->lupin = $lupin;

        return $this;
    }

    public function getMollusques(): ?string
    {
        return $this->mollusques;
    }

    public function setMollusques(?string $mollusques): self
    {
        $this->mollusques = $mollusques;

        return $this;
    }
}
