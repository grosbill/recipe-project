<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecettesRepository")
 */
class Recettes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slugrecette;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $prixrecette;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbdepersonne;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $noterecette;

    /**
     * @ORM\Column(type="text")
     */
    private $instructionrecette;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $descriptionrecette;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\RegimeAlimentaire", inversedBy="recettes")
     */
    private $regimealimentaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeRecette", inversedBy="recettes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typerecette;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TimeRecette", inversedBy="recettes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $timerecette;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Photo", mappedBy="recettes", orphanRemoval=true)
     */
    private $photo;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\RegionRecette", inversedBy="recettes")
     */
    private $regionrecette;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\FavorisRecette", inversedBy="recettes")
     */
    private $favorisrecette;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Difficulter", inversedBy="recettes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $difficulter;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="recettes")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ingredients", inversedBy="recettes")
     */
    private $ingredients;

    public function __construct()
    {
        $this->regimealimentaire = new ArrayCollection();
        $this->photo = new ArrayCollection();
        $this->regionrecette = new ArrayCollection();
        $this->favorisrecette = new ArrayCollection();
        $this->ingredients = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlugrecette(): ?string
    {
        return $this->slugrecette;
    }

    public function setSlugrecette(string $slugrecette): self
    {
        $this->slugrecette = $slugrecette;

        return $this;
    }

    public function getPrixrecette(): ?string
    {
        return $this->prixrecette;
    }

    public function setPrixrecette(string $prixrecette): self
    {
        $this->prixrecette = $prixrecette;

        return $this;
    }

    public function getNbdepersonne(): ?int
    {
        return $this->nbdepersonne;
    }

    public function setNbdepersonne(int $nbdepersonne): self
    {
        $this->nbdepersonne = $nbdepersonne;

        return $this;
    }

    public function getNoterecette(): ?string
    {
        return $this->noterecette;
    }

    public function setNoterecette(?string $noterecette): self
    {
        $this->noterecette = $noterecette;

        return $this;
    }

    public function getInstructionrecette(): ?string
    {
        return $this->instructionrecette;
    }

    public function setInstructionrecette(string $instructionrecette): self
    {
        $this->instructionrecette = $instructionrecette;

        return $this;
    }

    public function getDescriptionrecette(): ?string
    {
        return $this->descriptionrecette;
    }

    public function setDescriptionrecette(?string $descriptionrecette): self
    {
        $this->descriptionrecette = $descriptionrecette;

        return $this;
    }

    /**
     * @return Collection|RegimeAlimentaire[]
     */
    public function getRegimealimentaire(): Collection
    {
        return $this->regimealimentaire;
    }

    public function addRegimealimentaire(RegimeAlimentaire $regimealimentaire): self
    {
        if (!$this->regimealimentaire->contains($regimealimentaire)) {
            $this->regimealimentaire[] = $regimealimentaire;
        }

        return $this;
    }

    public function removeRegimealimentaire(RegimeAlimentaire $regimealimentaire): self
    {
        if ($this->regimealimentaire->contains($regimealimentaire)) {
            $this->regimealimentaire->removeElement($regimealimentaire);
        }

        return $this;
    }

    public function getTyperecette(): ?TypeRecette
    {
        return $this->typerecette;
    }

    public function setTyperecette(?TypeRecette $typerecette): self
    {
        $this->typerecette = $typerecette;

        return $this;
    }

    public function getTimerecette(): ?TimeRecette
    {
        return $this->timerecette;
    }

    public function setTimerecette(?TimeRecette $timerecette): self
    {
        $this->timerecette = $timerecette;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhoto(): Collection
    {
        return $this->photo;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photo->contains($photo)) {
            $this->photo[] = $photo;
            $photo->setRecettes($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photo->contains($photo)) {
            $this->photo->removeElement($photo);
            // set the owning side to null (unless already changed)
            if ($photo->getRecettes() === $this) {
                $photo->setRecettes(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RegionRecette[]
     */
    public function getRegionrecette(): Collection
    {
        return $this->regionrecette;
    }

    public function addRegionrecette(RegionRecette $regionrecette): self
    {
        if (!$this->regionrecette->contains($regionrecette)) {
            $this->regionrecette[] = $regionrecette;
        }

        return $this;
    }

    public function removeRegionrecette(RegionRecette $regionrecette): self
    {
        if ($this->regionrecette->contains($regionrecette)) {
            $this->regionrecette->removeElement($regionrecette);
        }

        return $this;
    }

    /**
     * @return Collection|FavorisRecette[]
     */
    public function getFavorisrecette(): Collection
    {
        return $this->favorisrecette;
    }

    public function addFavorisrecette(FavorisRecette $favorisrecette): self
    {
        if (!$this->favorisrecette->contains($favorisrecette)) {
            $this->favorisrecette[] = $favorisrecette;
        }

        return $this;
    }

    public function removeFavorisrecette(FavorisRecette $favorisrecette): self
    {
        if ($this->favorisrecette->contains($favorisrecette)) {
            $this->favorisrecette->removeElement($favorisrecette);
        }

        return $this;
    }

    public function getDifficulter(): ?Difficulter
    {
        return $this->difficulter;
    }

    public function setDifficulter(?Difficulter $difficulter): self
    {
        $this->difficulter = $difficulter;

        return $this;
    }

    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Ingredients[]
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredients $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
        }

        return $this;
    }

    public function removeIngredient(Ingredients $ingredient): self
    {
        if ($this->ingredients->contains($ingredient)) {
            $this->ingredients->removeElement($ingredient);
        }

        return $this;
    }
}
