<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190919135533 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE allergenes CHANGE cerales cerales VARCHAR(50) DEFAULT NULL, CHANGE crustaces crustaces VARCHAR(50) DEFAULT NULL, CHANGE oeufs oeufs VARCHAR(50) DEFAULT NULL, CHANGE poissons poissons VARCHAR(50) DEFAULT NULL, CHANGE arachides arachides VARCHAR(50) DEFAULT NULL, CHANGE soja soja VARCHAR(50) DEFAULT NULL, CHANGE lait lait VARCHAR(50) DEFAULT NULL, CHANGE fruitsacoques fruitsacoques VARCHAR(50) DEFAULT NULL, CHANGE celeri celeri VARCHAR(50) DEFAULT NULL, CHANGE moutarde moutarde VARCHAR(50) DEFAULT NULL, CHANGE grainedesesame grainedesesame VARCHAR(50) DEFAULT NULL, CHANGE anhydridesulfite anhydridesulfite VARCHAR(50) DEFAULT NULL, CHANGE lupin lupin VARCHAR(50) DEFAULT NULL, CHANGE mollusques mollusques VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE pays_recette CHANGE paysrecette paysrecette VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE photo CHANGE adressephoto adressephoto VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE recettes ADD nom VARCHAR(50) NOT NULL, CHANGE users_id users_id INT DEFAULT NULL, CHANGE noterecette noterecette VARCHAR(100) DEFAULT NULL, CHANGE descriptionrecette descriptionrecette VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE regime_alimentaire CHANGE valeurregime valeurregime VARCHAR(25) DEFAULT NULL');
        $this->addSql('ALTER TABLE region_recette CHANGE region region VARCHAR(50) DEFAULT NULL, CHANGE coderegion coderegion INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stock CHANGE users_id users_id INT DEFAULT NULL, CHANGE valeurstock valeurstock DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE time_recette CHANGE timecuisson timecuisson TIME DEFAULT NULL');
        $this->addSql('ALTER TABLE type_recette CHANGE typederecette typederecette VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users CHANGE roles roles JSON NOT NULL, CHANGE usersrecette usersrecette VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE allergenes CHANGE cerales cerales VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE crustaces crustaces VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE oeufs oeufs VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE poissons poissons VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE arachides arachides VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE soja soja VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE lait lait VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE fruitsacoques fruitsacoques VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE celeri celeri VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE moutarde moutarde VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE grainedesesame grainedesesame VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE anhydridesulfite anhydridesulfite VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE lupin lupin VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE mollusques mollusques VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE pays_recette CHANGE paysrecette paysrecette VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE photo CHANGE adressephoto adressephoto VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE recettes DROP nom, CHANGE users_id users_id INT DEFAULT NULL, CHANGE noterecette noterecette VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE descriptionrecette descriptionrecette VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE regime_alimentaire CHANGE valeurregime valeurregime VARCHAR(25) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE region_recette CHANGE region region VARCHAR(50) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE coderegion coderegion INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stock CHANGE users_id users_id INT DEFAULT NULL, CHANGE valeurstock valeurstock DOUBLE PRECISION DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE time_recette CHANGE timecuisson timecuisson TIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE type_recette CHANGE typederecette typederecette VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE users CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin, CHANGE usersrecette usersrecette VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
    }
}
