<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190919135044 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE aliments (id INT AUTO_INCREMENT NOT NULL, nomaliment VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE allergenes (id INT AUTO_INCREMENT NOT NULL, cerales VARCHAR(50) DEFAULT NULL, crustaces VARCHAR(50) DEFAULT NULL, oeufs VARCHAR(50) DEFAULT NULL, poissons VARCHAR(50) DEFAULT NULL, arachides VARCHAR(50) DEFAULT NULL, soja VARCHAR(50) DEFAULT NULL, lait VARCHAR(50) DEFAULT NULL, fruitsacoques VARCHAR(50) DEFAULT NULL, celeri VARCHAR(50) DEFAULT NULL, moutarde VARCHAR(50) DEFAULT NULL, grainedesesame VARCHAR(50) DEFAULT NULL, anhydridesulfite VARCHAR(50) DEFAULT NULL, lupin VARCHAR(50) DEFAULT NULL, mollusques VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE difficulter (id INT AUTO_INCREMENT NOT NULL, niveaux VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE favoris_recette (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pays_recette (id INT AUTO_INCREMENT NOT NULL, paysrecette VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo (id INT AUTO_INCREMENT NOT NULL, recettes_id INT NOT NULL, adressephoto VARCHAR(255) DEFAULT NULL, INDEX IDX_14B784183E2ED6D6 (recettes_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recettes (id INT AUTO_INCREMENT NOT NULL, typerecette_id INT NOT NULL, timerecette_id INT NOT NULL, difficulter_id INT NOT NULL, users_id INT DEFAULT NULL, ingredientrecette VARCHAR(255) NOT NULL, slugrecette VARCHAR(255) NOT NULL, prixrecette VARCHAR(50) NOT NULL, nbdepersonne INT NOT NULL, noterecette VARCHAR(100) DEFAULT NULL, instructionrecette LONGTEXT NOT NULL, descriptionrecette VARCHAR(50) DEFAULT NULL, INDEX IDX_EB48E72C706BCFE4 (typerecette_id), INDEX IDX_EB48E72C56DB852C (timerecette_id), INDEX IDX_EB48E72CCB4FD4B1 (difficulter_id), INDEX IDX_EB48E72C67B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recettes_regime_alimentaire (recettes_id INT NOT NULL, regime_alimentaire_id INT NOT NULL, INDEX IDX_12F8110A3E2ED6D6 (recettes_id), INDEX IDX_12F8110A7B2EE792 (regime_alimentaire_id), PRIMARY KEY(recettes_id, regime_alimentaire_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recettes_region_recette (recettes_id INT NOT NULL, region_recette_id INT NOT NULL, INDEX IDX_F70AF7C93E2ED6D6 (recettes_id), INDEX IDX_F70AF7C9A8D61B08 (region_recette_id), PRIMARY KEY(recettes_id, region_recette_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recettes_favoris_recette (recettes_id INT NOT NULL, favoris_recette_id INT NOT NULL, INDEX IDX_9D9F96A33E2ED6D6 (recettes_id), INDEX IDX_9D9F96A3B0E0E171 (favoris_recette_id), PRIMARY KEY(recettes_id, favoris_recette_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE regime_alimentaire (id INT AUTO_INCREMENT NOT NULL, valeurregime VARCHAR(25) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region_recette (id INT AUTO_INCREMENT NOT NULL, region VARCHAR(50) DEFAULT NULL, coderegion INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stock (id INT AUTO_INCREMENT NOT NULL, users_id INT DEFAULT NULL, valeurstock DOUBLE PRECISION DEFAULT NULL, INDEX IDX_4B36566067B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE time_recette (id INT AUTO_INCREMENT NOT NULL, timepreparation TIME NOT NULL, timecuisson TIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_aliment (id INT AUTO_INCREMENT NOT NULL, nomtypealiment VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_recette (id INT AUTO_INCREMENT NOT NULL, typederecette VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, lastname VARCHAR(50) NOT NULL, fisrtname VARCHAR(50) NOT NULL, pseudo VARCHAR(50) NOT NULL, usersrecette VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_favoris_recette (users_id INT NOT NULL, favoris_recette_id INT NOT NULL, INDEX IDX_4724350F67B3B43D (users_id), INDEX IDX_4724350FB0E0E171 (favoris_recette_id), PRIMARY KEY(users_id, favoris_recette_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B784183E2ED6D6 FOREIGN KEY (recettes_id) REFERENCES recettes (id)');
        $this->addSql('ALTER TABLE recettes ADD CONSTRAINT FK_EB48E72C706BCFE4 FOREIGN KEY (typerecette_id) REFERENCES type_recette (id)');
        $this->addSql('ALTER TABLE recettes ADD CONSTRAINT FK_EB48E72C56DB852C FOREIGN KEY (timerecette_id) REFERENCES time_recette (id)');
        $this->addSql('ALTER TABLE recettes ADD CONSTRAINT FK_EB48E72CCB4FD4B1 FOREIGN KEY (difficulter_id) REFERENCES difficulter (id)');
        $this->addSql('ALTER TABLE recettes ADD CONSTRAINT FK_EB48E72C67B3B43D FOREIGN KEY (users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE recettes_regime_alimentaire ADD CONSTRAINT FK_12F8110A3E2ED6D6 FOREIGN KEY (recettes_id) REFERENCES recettes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recettes_regime_alimentaire ADD CONSTRAINT FK_12F8110A7B2EE792 FOREIGN KEY (regime_alimentaire_id) REFERENCES regime_alimentaire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recettes_region_recette ADD CONSTRAINT FK_F70AF7C93E2ED6D6 FOREIGN KEY (recettes_id) REFERENCES recettes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recettes_region_recette ADD CONSTRAINT FK_F70AF7C9A8D61B08 FOREIGN KEY (region_recette_id) REFERENCES region_recette (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recettes_favoris_recette ADD CONSTRAINT FK_9D9F96A33E2ED6D6 FOREIGN KEY (recettes_id) REFERENCES recettes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recettes_favoris_recette ADD CONSTRAINT FK_9D9F96A3B0E0E171 FOREIGN KEY (favoris_recette_id) REFERENCES favoris_recette (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B36566067B3B43D FOREIGN KEY (users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users_favoris_recette ADD CONSTRAINT FK_4724350F67B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_favoris_recette ADD CONSTRAINT FK_4724350FB0E0E171 FOREIGN KEY (favoris_recette_id) REFERENCES favoris_recette (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE recettes DROP FOREIGN KEY FK_EB48E72CCB4FD4B1');
        $this->addSql('ALTER TABLE recettes_favoris_recette DROP FOREIGN KEY FK_9D9F96A3B0E0E171');
        $this->addSql('ALTER TABLE users_favoris_recette DROP FOREIGN KEY FK_4724350FB0E0E171');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B784183E2ED6D6');
        $this->addSql('ALTER TABLE recettes_regime_alimentaire DROP FOREIGN KEY FK_12F8110A3E2ED6D6');
        $this->addSql('ALTER TABLE recettes_region_recette DROP FOREIGN KEY FK_F70AF7C93E2ED6D6');
        $this->addSql('ALTER TABLE recettes_favoris_recette DROP FOREIGN KEY FK_9D9F96A33E2ED6D6');
        $this->addSql('ALTER TABLE recettes_regime_alimentaire DROP FOREIGN KEY FK_12F8110A7B2EE792');
        $this->addSql('ALTER TABLE recettes_region_recette DROP FOREIGN KEY FK_F70AF7C9A8D61B08');
        $this->addSql('ALTER TABLE recettes DROP FOREIGN KEY FK_EB48E72C56DB852C');
        $this->addSql('ALTER TABLE recettes DROP FOREIGN KEY FK_EB48E72C706BCFE4');
        $this->addSql('ALTER TABLE recettes DROP FOREIGN KEY FK_EB48E72C67B3B43D');
        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B36566067B3B43D');
        $this->addSql('ALTER TABLE users_favoris_recette DROP FOREIGN KEY FK_4724350F67B3B43D');
        $this->addSql('DROP TABLE aliments');
        $this->addSql('DROP TABLE allergenes');
        $this->addSql('DROP TABLE difficulter');
        $this->addSql('DROP TABLE favoris_recette');
        $this->addSql('DROP TABLE pays_recette');
        $this->addSql('DROP TABLE photo');
        $this->addSql('DROP TABLE recettes');
        $this->addSql('DROP TABLE recettes_regime_alimentaire');
        $this->addSql('DROP TABLE recettes_region_recette');
        $this->addSql('DROP TABLE recettes_favoris_recette');
        $this->addSql('DROP TABLE regime_alimentaire');
        $this->addSql('DROP TABLE region_recette');
        $this->addSql('DROP TABLE stock');
        $this->addSql('DROP TABLE time_recette');
        $this->addSql('DROP TABLE type_aliment');
        $this->addSql('DROP TABLE type_recette');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE users_favoris_recette');
    }
}
