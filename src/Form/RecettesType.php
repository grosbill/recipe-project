<?php

namespace App\Form;

use App\Entity\Recettes;
use App\Entity\Ingredients;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecettesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('ingredients', EntityType::class, [
                'label' => 'Ingrédients :',
                'class'=>Ingredients::Class,
                'choice_label' =>'nom',
                'multiple' => true,'expanded' => true, 
            ])
            ->add('slugrecette')
            ->add('prixrecette')
            ->add('nbdepersonne')
            ->add('noterecette')
            ->add('instructionrecette')
            ->add('descriptionrecette')
            ->add('regimealimentaire')
            ->add('typerecette')
            ->add('timerecette')
            ->add('regionrecette')
            ->add('difficulter')
            ->add('users')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Recettes::class,
        ]);
    }
}
